Update JIRA tickets moved on a JimFlow Kanban board. See
http://jimflow.jimdo.com.

This replaces the PHP cron job in JimFlowWall that is designed to run after
JimFlowKlopfer. It uses the outputs of JimFlowKlopfer.

Usage::

  $ jirathumper --url=<JIRA URL> --username=<username> --password=<password> --config=config.ini klopfer-output.json

The configuration file (e.g. `config.ini`) contains::

    [columns]
    0 = todo
    1 = in-progress
    2 = done

This maps column indexes (0 is the first one) to the titles of JIRA statuses.

The input file is produced by JimFlowKlopfer. It maps the current position
(column index) of each item on the board, as identified by JIRA ticket ids.

jirathumper will then look up each ticket in JIRA and identify its current
status. If this is diffent from the one indicated by the columns on the wall,
it will look for possible transitions to take it into that state. If such a
transition exists, it will be executed.